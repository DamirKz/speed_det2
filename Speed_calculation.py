# -*- coding: utf-8 -*-
from utils.utility_types import TVector, TImagePoint, TFolder, TFile
from configurator import Config
import numpy as np
import cv2
import os
import _pickle as cPickle
#import pickle
import sys
import matplotlib.pyplot as plt
import json
import math

"""
Standart axis:
        Axis directions
                                   y
             ----------------------
             |
             |
             |    Image 0
           x |
             |
x1y1_x2y2_coordinates: should be in the form [ [x1, y1], [x2, y2]]
"""

def transform_xy_to_point(standart_point, M, Shift)-> list:  # transform point in standart coordinate and return standart coordinate

    ar = np.float32([standart_point[1], standart_point[0], 1])
    p = np.matmul(M, ar)
    px, py = p[0] / p[2], p[1] / p[2]
    px = px + Shift[1]
    py = py + Shift[0]
    return [py, px]

def cv2_to_standart(input_point: list) -> list:

    x, y = input_point[0], input_point[1]
    return [y,x]

def show_image(image_mat: np.ndarray):
    while (1):
        cv2.imshow('image', image_mat)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()


if __name__ == '__main__':

    print('Speed Calculation')

    root_input = Config.module_tests_root
    input_dir, output_dir, debug_dir = Config.get_all_module_tests_dirs('speed_calculation')

    name_sample_dir = 'session3_left'
    calib_json = 'Info_Calibration.json'
    input_ID_json  = 'ID5.json' # input json with tracking ID car
    output_ID_json = os.path.splitext(input_ID_json)
    output_ID_json = output_ID_json[0]+'_speed' + output_ID_json[1]
    show_id = True

    sample_output_folder = output_dir.add_sub_path(name_sample_dir).make()
    sample_debug_folder = debug_dir.add_sub_path(name_sample_dir).make()
    sample_input_folder = os.path.join(input_dir, name_sample_dir)

    calib_json_path = os.path.join(sample_input_folder, calib_json)
    ID_json_path = os.path.join(sample_input_folder, input_ID_json)
    sample_png = os.path.join(sample_input_folder, 'debug.png')
    output_ID_path = os.path.join(sample_output_folder, output_ID_json)

    print(' --------  Base Dir -------------')
    print(input_dir)
    print(output_dir)
    print(debug_dir)
    print(' --------  Input, Output, Debag Dirs -------------')
    print(sample_input_folder)
    print(sample_output_folder)
    print(sample_debug_folder)
    print(' --------  Input, Output, Debag Files -------------')
    print(calib_json_path)
    print(ID_json_path)
    print(sample_png)
    print(output_ID_path)

    # load json with calibration data
    with open(calib_json_path) as f:
        info_ = json.load(f)

    M = np.array(info_['HomographyMatrix'])
    Shift = np.array(info_['Shift'])
    TransformedPoints = info_['Transformed_Input_Points']
    x_m = info_['XY_m'][0]
    y_m = info_['XY_m'][1]
    fps = info_['fps']

    # Load json with track of ID
    ID_data = {}
    with open(ID_json_path) as f:
        ID_data = json.load(f)

    if show_id == True:
        image_mat = cv2.imread(sample_png)
        for idx, df in enumerate(TransformedPoints):
            cv2.circle(image_mat, (df[1], df[0]), 5, (255, 0, 0), -1)
            if (idx == 1)or (idx == 3):
                cv2.circle(image_mat, (df[1], df[0]), 5, (255, 255, 0), -1)

    id0_frame = ID_data[0]['origin_frame']
    for id in ID_data:
        if id['origin_frame'] >= id0_frame:
            id0_frame = id['origin_frame']
        else:
            print('Error! Frames are out of order')
            break

    ID_data_speed = ID_data
    num_of_frame = 0
    for idx, id in enumerate(ID_data_speed):

        x = id['X'] - np.float32(id['Width']) / 2
        y = id['Y'] - np.float32(id['Height']) / 2
        standart_point = cv2_to_standart([x, y]) # convert cv2 to standart point
        transform_point = transform_xy_to_point(standart_point, M, Shift) # standart point
        if (transform_point[0] > TransformedPoints[1][0])and(transform_point[0] < TransformedPoints[3][0]): # control intersection
            cv2.circle(image_mat, (int(transform_point[1]), int(transform_point[0])), 2, (255, 0, 255), -1)
            num_of_frame = num_of_frame + 1
            if num_of_frame == 1:
                start_point = (transform_point[0], transform_point[1])
                ID_data_speed[idx]['Speed'] = -1
            if num_of_frame > 1:
                dx_m = (transform_point[0] - start_point[0])*x_m  # distance in m in axis X
                dy_m = (transform_point[1] - start_point[1])*y_m  # distance in m in axis Y
                distance_traveled = math.sqrt(dx_m**2 + dy_m**2)
                time_delta = (num_of_frame - 1) * (1. / fps)
                speed = 3.6*distance_traveled/time_delta   # km/h
                ID_data_speed[idx]['Speed'] = speed
        else:
            cv2.circle(image_mat, (int(transform_point[1]), int(transform_point[0])), 2, (255, 255, 255), -1)
            ID_data_speed[idx]['Speed'] = -1

    with open(output_ID_path, "w") as data_file:
        json.dump(ID_data_speed, data_file, indent=4)

    show_image(image_mat)

    # sc = 0
    # fps = 50
    # for key in my_data:
    #     #print(key)
    #     x = my_data[key]['X'] + my_data[key]['Width']/2
    #     y = my_data[key]['Y'] + my_data[key]['Height']/2
    #     standart_point = cv2_to_standart([x,y])
    #     transform_point = transform_xy_to_point(standart_point, M, Shift)
    #     transform_data[key]['X'] = transform_point[1]  # from standart to cv
    #     transform_data[key]['Y'] = transform_point[0]
    #
    #     if (transform_point[0] > TransformedPoints[1][0])and(transform_point[0] < TransformedPoints[3][0]):
    #         cv2.circle(image_mat, (int(transform_point[1]), int(transform_point[0])), 2, (255, 0, 255), -1)
    #         sc = sc + 1
    #     else:
    #         cv2.circle(image_mat, (int(transform_point[1]), int(transform_point[0])), 2, (255, 255, 255), -1)
    # name_session = 'session2_left/'
    # path = 'D:/VideosetsBRNO/BrnoCompSpeed-full/BrnoCompSpeed/dataset/'
    # sample_name = path + name_session + 'Part1_364.json'
    # ID_name_json = path + name_session + 'Part1_364_ID4.json'
    # matrix_filename = path + name_session + 'Homography_Matrix.txt'
    # debug_filename = path + name_session + 'debug.png'
    # shift_filename = path + name_session + 'Shift.txt'
    # json_filename = path + name_session + 'Info_Calibration.json'


    # with open(json_filename) as f:
    #     info_ = json.load(f)
    #
    # M = np.array(info_['HomographyMatrix'])
    # Shift = np.array(info_['Shift'])
    # x_m = info_['x_m']
    # y_m = info_['y_m']
    # TransformedPoints = info_['Transformed Calibration Points']
    #
    # # Load json with track of ID
    # my_data = {}
    # with open(ID_name_json) as f:
    #     my_data = json.load(f)
    # transform_data = my_data
    #
    # image_mat = cv2.imread(debug_filename)
    #
    # for df in TransformedPoints:
    #     cv2.circle(image_mat, (df[1], df[0]), 5, (255, 0, 0), -1)
    #
    # sc = 0
    # fps = 50
    # for key in my_data:
    #     #print(key)
    #     x = my_data[key]['X'] + my_data[key]['Width']/2
    #     y = my_data[key]['Y'] + my_data[key]['Height']/2
    #     standart_point = cv2_to_standart([x,y])
    #     transform_point = transform_xy_to_point(standart_point, M, Shift)
    #     transform_data[key]['X'] = transform_point[1]  # from standart to cv
    #     transform_data[key]['Y'] = transform_point[0]
    #
    #     if (transform_point[0] > TransformedPoints[1][0])and(transform_point[0] < TransformedPoints[3][0]):
    #         cv2.circle(image_mat, (int(transform_point[1]), int(transform_point[0])), 2, (255, 0, 255), -1)
    #         sc = sc + 1
    #     else:
    #         cv2.circle(image_mat, (int(transform_point[1]), int(transform_point[0])), 2, (255, 255, 255), -1)
    #
    # #
    # print(sc)
    # time_to_track = sc/fps
    # speed = (28/time_to_track)*3.6
    # real_speed = 96.68
    # error = 100*abs(speed-real_speed)/real_speed
    # print(speed, error)
    #
    # cv2.imwrite(debug_filename, image_mat)
    # show_image(image_mat)
    # # print(transform_data)



