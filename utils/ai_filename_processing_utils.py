import copy
import unittest
import warnings
import numpy as np
import os
import re

def sort_human_way(l: list):
    """ Sort the given list in place the way that humans expect.
    """
    def tryint(s):
        try:
            return int(s)
        except ValueError:
            return s
    def alphanum_key(s):
        """ Turn a string into a list of string and number chunks.
            "z23a" -> ["z", 23, "a"]
        """
        return [tryint(c) for c in re.split('([0-9]+)', s)]
    l.sort(key=alphanum_key)

# TODO add unit tests
def get_xyz_from_files(first_file: str, last_file: str,
                       width: int, height: int) -> (np.ndarray, np.ndarray):
    """

    :param first_file: F758_77.028__rec00000174.tif
    :param last_file: F758_77.028__rec00001188.tif
    :param width: 748
    :param height: 764
    :return: size_xyz = [ 764  748 1015], start_xyz = [  0   0 174]
    """
    # find digits number in the end of file, ex. 'F816_3.651__rec00000230.tif' -> 8
    # what to do if we have 0.png, 1.png, etc...
    without_extension_1, file_extension = os.path.splitext(first_file)
    without_extension_2, file_extension = os.path.splitext(last_file)
    if without_extension_1.isdigit() and without_extension_2.isdigit():
        # print('No extension: ', int(without_extension_1), int(without_extension_2))

        # it is the case of 0.png, 1000.png
        z_first = int(without_extension_1)
        z_last = int(without_extension_2)

        # calculate size and start coordinates
        x_size = height
        y_size = width
        size_xyz = np.array([x_size, y_size, z_last - z_first + 1])
        start_xyz = np.array([0, 0, z_first])

        return size_xyz, start_xyz

    digits_number = 0
    if not digits_number:
        digits_number = 2
        without_extension, file_extension = os.path.splitext(first_file)
        while without_extension[-digits_number:].isdigit():
            digits_number += 1
        digits_number = digits_number - 1

    # get first and last z
    # print(digits_number)
    first_without_extension, file_extension = os.path.splitext(first_file)
    last_without_extension, file_extension = os.path.splitext(last_file)
    z_first = int(first_without_extension[-digits_number:])
    z_last = int(last_without_extension[-digits_number:])

    # calculate size and start coordinates
    x_size = height
    y_size = width
    size_xyz = np.array([x_size, y_size, z_last - z_first + 1])
    start_xyz = np.array([0, 0, z_first])

    return size_xyz, start_xyz

def best_name_correspondence(sample_name: str, candidates: list) -> str:
    """
    Intellectual name finding, takes a name, and searches best match among possible candidates
    :param sample_name: 'F758_77.028__rec'
    :param candidates: ['F758-AB_4.587_ORDERED', 'F758_77.028_Rec', 'F758_77.028_Rec_Meta', 'F758_77.028_Rec_ORDERED']
    :return: best matching one: 'F758_77.028_Rec'
    """

    original = sample_name

    # removing rec at the end, if exists.
    if original[-3:] == 'rec' or original[-3:] == 'Rec':
        original = original[:-3]

    # remove trailing underscore at the end
    while original[-1:] == '_':
        original = original[:-1]

    # find best matches
    short_list = [item for item in candidates if original in item]

    # take the shortest, usually it works, ex: ['F758_77.028_Rec', 'F758_77.028_Rec_Meta', 'F758_77.028_Rec_ORDERED']
    if not short_list:
        return ''
    best_match = min(short_list, key=len)

    return str(best_match)

def find_most_common_extension(list_of_files: list) -> str:
    # take last 4, in case that we have jpeg
    extensions = [item[-4:] for item in list_of_files]

    # find most common extension
    most_common = max(set(extensions), key=extensions.count)

    # in case that we have .tif, or .png -> tif or png
    if most_common[0] == '.':
        most_common = most_common[1:]

    return most_common

def end_with_digits_and_extension(list_of_files: list,
                                  extension: str = 'tif',
                                  digits_number: int = 2) ->list:
    """
    Returns a filtered list with files only with specific extension and ending with several digits
    :param list_of_files:
    :param extension:
    :param digits_number:
    :return: filtered list
    """
    MINIMUM_SENSIBLE_DIGITS_NUMBER = 2
    if digits_number < MINIMUM_SENSIBLE_DIGITS_NUMBER:
        warnings.warn('digits_number is {0}, it is less than a sensible number: '
                      '{1}, check source code'.format(digits_number, MINIMUM_SENSIBLE_DIGITS_NUMBER))

    filtered_list = [file for file in list_of_files if file.endswith(extension)]
    ends_with_digits = []
    extension_and_dot_len = len(extension) + 1
    for file in filtered_list:
        should_be_digit = file[-extension_and_dot_len - digits_number:-extension_and_dot_len]
        if should_be_digit.isdigit():
            ends_with_digits.append(file)
    return ends_with_digits

class TestAll(unittest.TestCase):

    def test_best_name_correspondence(self):
        # names_with_additions = ['F758-AB_4.587__rec', 'F758_77.028__rec', 'F816_3.651__rec']
        sample_names = ['F758-AB_4.587_ORDERED', 'F758-AB_4.587_Rec',
                        'F758-AB_4.587_Rec_Meta', 'F758_77.028_R',
                        'F758_77.028_Rec_Meta', 'F758_77.028_Rec_ORDERED',
                        'F816_3.651_Rec', 'Full_Stone_First']
        stone_name = 'F758_77.028__rec'
        must_find = 'F758_77.028_R'
        best_match = best_name_correspondence(stone_name, sample_names)  # sample_names[3]
        self.assertTrue(best_match == must_find)

    def test_end_with_digits_and_extension(self):
        test_files = ['7c66d0f9-0c28-4bb9-97a3-5b5f4f936dab.iddata', 'aidataArchive_20180512_112202.zip',
                      'F816_3.651_3D_grow cut.mme', 'F816_3.651__pp1.tif', 'F816_3.651__pp2.tif',
                      'F816_3.651__rec.log', 'F816_3.651__rec00000230.tif', 'F816_3.651__rec00000231.tif',
                      'F816_3.651__rec00001145.tif', 'F816_3.651__rec_spr.bmp', 'F816_3.651__rec_spr.tif']
        result = end_with_digits_and_extension(test_files, digits_number=4)
        must_find = ['F816_3.651__rec00000230.tif', 'F816_3.651__rec00000231.tif', 'F816_3.651__rec00001145.tif']
        self.assertTrue(result == must_find)

    def test_get_xyz_from_files(self):
        # test input
        first_file = 'F758_77.028__rec00000174.tif'
        last_file = 'F758_77.028__rec00001188.tif'
        width = 748
        height = 764

        # test output
        size_output = np.array([ 764,  748, 1015])
        start_xyz_output = np.array([ 0, 0, 174])

        # calculated
        calculated_size, calculated_start = get_xyz_from_files(first_file, last_file, width, height)

        self.assertTrue(np.array_equal(calculated_size, size_output) and
                        np.array_equal(calculated_start, start_xyz_output))

    def test_find_most_common_extension(self):
        list_of_files = ['056bcff2-a693-41b3-9753-923103b2d978.iddata', 'F758_77.028.mme',
                         'F758_77.028_3D grow-cut.mme', 'F758_77.028__pp1.tif', 'F758_77.028__pp2.tif',
                         'F758_77.028__rec.log', 'F758_77.028__rec00000174.tif',
                         'F758_77.028__rec00000175.tif', 'F758_77.028__rec00000176.tif',
                         'F758_77.028__rec00000177.tif', 'F758_77.028__rec00000178.tif',
                         'F758_77.028__rec00000179.tif', 'F758_77.028__rec00000180.tif',
                         'F758_77.028__rec00000181.tif', 'F758_77.028__rec00000182.tif',
                         'F758_77.028__rec00000183.tif',
                         'F758_77.028__rec00000184.tif', 'F758_77.028__rec00000185.tif']
        result = find_most_common_extension(list_of_files)
        canonic = 'tif'
        self.assertTrue(result == canonic)

if __name__ == '__main__':

    print('Some intellectual processing routines')
    unittest.main()


