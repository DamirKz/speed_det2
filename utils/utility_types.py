"""
Custom types holder, I plan to switch gradually to typed representation.
"""
import json
import os

import numpy as np
from functools import reduce
# NOTE should not import anything of higher level, to exclude circular dependency
from utils import ai_filename_processing_utils as ai_fn

# TODO this does not work from console, you may be should consider changing this, by
# fixme changing project structure,
# fixme example: https://stackoverflow.com/questions/714063/importing-modules-from-parent-folder/50194143#50194143

class TColor(object):

    _DEFINED_COLORS = {'RED':    np.array([255,   0,   0]),
                       'GREEN':  np.array([  0, 255,   0]),
                       'BLUE':   np.array([  0,   0, 255]),
                       'YELLOW': np.array([255, 255,   0]),
                       'PURPLE': np.array([163,  73, 164]),
                       'BLACK':  np.array([  0,   0,   0]),
                       'WHITE':  np.array([255, 255, 255])
                      }

    @classmethod
    def get_specific_color(cls, color_name: str):
        if color_name not in cls._DEFINED_COLORS.keys():
            raise ValueError('This color is not supported')

        return cls(cls._DEFINED_COLORS[color_name])

    @classmethod
    def get_random_color(cls):
        set_to_zero = np.random.randint(3)
        color = np.random.randint(100, 255, size=3)
        color[set_to_zero] = 0
        return cls(color)   # зачем оборачивать?? возвращает объект Tcolor

    def __init__(self, rgb_color: np.ndarray):
        self.as_array = rgb_color

    @property
    def red(self):
        return self.as_array[0]

    @property
    def green(self):
        return self.as_array[1]

    @property
    def blue(self):
        return self.as_array[2]

class TImagePoint(np.ndarray):

    """
            Axis directions
            (0, 0)                       y
                 ---------------------->
                 |
                 |
                 |    Image 0
               x |
                 |
                 V
    """

    @classmethod
    def zero(cls):
        return cls([0, 0])

    @classmethod
    def from_np_array(cls, point_2d: np.ndarray):
        return cls([point_2d[0], point_2d[1]])


    def __new__(cls, xy_coordinates: list):
        """
        vector subclass
        :param xyz_coordiantes: [1, 2]
        :return:
        """
        as_numpy = np.array(xy_coordinates)
        assert as_numpy.shape == (2,), 'SHOULD be pure 2d vector'
        return as_numpy.view(cls)  # ????????

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]


class TVector(np.ndarray):

    @classmethod
    def zero(cls):
        return cls([0, 0, 0])

    @classmethod
    def from_np_array(cls, vector: np.ndarray):
        return cls([vector[0], vector[1], vector[2]])

    def __new__(cls, xyz_coordiantes: list):
        """
        vector subclass
        :param xyz_coordiantes: [1, 2, 3]
        :return:
        """
        as_numpy = np.array(xyz_coordiantes)
        assert as_numpy.shape == (3,), 'SHOULD be pure 3d vector'
        return as_numpy.view(cls)

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]

    @property
    def as_list(self):
        return [int(self[0]), int(self[1]), int(self[2])]

class TSize(np.ndarray):

    @classmethod
    def zero(cls):
        return cls([0, 0, 0])

    def __new__(cls, xyz_size: list):
        """
        size subclass
        :param xyz_size: [1, 2, 3]
        :return:
        """
        as_numpy = np.array(xyz_size)
        assert as_numpy.shape == (3,), 'SHOULD be pure 3d vector'
        return as_numpy.view(cls)

class TFile(str):

    @classmethod
    def zero(cls):
        return cls('')

    def exists(self) -> bool:
        return os.path.isfile(self)

    @property
    def extension(self) -> str:
        filename, file_extension = os.path.splitext(self)
        return file_extension

    @property
    def only_filename(self) -> str:
        only_filename = os.path.basename(self)
        return only_filename

    @property
    def only_filename_without_extension(self) -> str: # -> TFile
        only_filename = os.path.basename(self)
        filename_without_extension, file_extension = os.path.splitext(only_filename)
        return filename_without_extension

    @property
    def only_folder(self): # -> TFolder: , cannot add directly, due to language limitations
        return TFolder(os.path.dirname(self))

    def replace_extension_with(self, extension: str):  # -> TFile
        standard_extension = extension
        if not extension.startswith('.'):
            standard_extension = '.' + extension
        new_filename = self.only_folder.add_file_name(self.only_filename_without_extension + standard_extension)
        return new_filename

    def add_extension(self, extension: str):  # -> TFile
        standard_extension = extension
        if not extension.startswith('.'):
            standard_extension = '.' + extension
        new_filename = self.only_folder.add_file_name(self.only_filename + standard_extension)
        return new_filename

    def folder_exists(self) -> bool:
        folder = TFolder(os.path.dirname(self))
        return folder.exists()

class TFolder(str):
    """
        Not sure if it is convinient to use, but I want to try this approach.
        Is a subtype of str, as it inherits from it, can be used in other places,
        "Liskov substitution"
    """
    @classmethod
    def zero(cls):
        return cls('')

    def make(self):
        """
        We make only if one level down exists.
        If exists - do nothing.
        :return:
        """
        level_up = os.path.dirname(self)
        if not os.path.isdir(level_up):
            raise ValueError('Folder one level should exist')

        os.makedirs(self, exist_ok=True)
        return self

    def add_file_name(self, file_name: str) -> TFile:
        return TFile(os.path.join(self, file_name))

    def add_filename(self, file_name: str) -> TFile:
        return TFile(os.path.join(self, file_name))

    def add_sub_path(self, sub_path: str):
        # check that sub path is not TFolder, as it is a typical usage mistake
        if isinstance(sub_path, TFolder):
            raise ValueError('Usage mistake, it is likely that you put the whole path, instead of subpath')
        return TFolder(os.path.join(self, sub_path))

    def add_subpath(self, sub_path: str):
        # check that sub path is not TFolder, as it is a typical usage mistake
        if isinstance(sub_path, TFolder):
            raise ValueError('Usage mistake, it is likely that you put the whole path, instead of subpath')
        return TFolder(os.path.join(self, sub_path))

    def get_level_up_path(self):
        return TFolder(os.path.dirname(self))

    def get_last_path(self) -> str:
        return os.path.basename(self)

    def exists(self) -> bool:
        return os.path.isdir(self)

    def get_only_files_with_extension(self, extension: str=''):  #  -> TFileList: cannot do directly due to language limitations
        """
        Returns list of only files in folder, sorted human way
        :param extension: if provided ignores other extensions
        :return:
        """
        dirs_and_files = os.listdir(self)
        only_files = [self.add_file_name(item) for item in dirs_and_files if os.path.isfile(os.path.join(self, item))]
        only_files = TFileList(only_files)
        only_files.sort_human_way()
        if extension:
            only_files_with_extension = only_files.only_with_extension(extension)
            return only_files_with_extension
        else:
            return only_files

class TFileList(list):   # ???

    def sort_human_way(self):
        ai_fn.sort_human_way(self)

    def __init__(self, files: list):
        as_TFile = [TFile(item) for item in files]
        super(TFileList, self).__init__(as_TFile)
        # can be aceesed as a list, through inheritance

        # verify that all of elements are TFolder type!!!
        for item in self:
            self._check(item)

    def only_with_extension(self, extension: str): # -> TFilesList
        if not extension:
            raise ValueError('There is some misunderstanding, as there should be a valid extension provided')
        # handle the case when provided 'npy', but for processing is needed '.npy'
        adapted_extension = extension
        if extension[0] != '.':
            adapted_extension = '.' + extension
        only_with_extension = [a_file for a_file in self if a_file.extension == adapted_extension]
        return only_with_extension

    def exists(self) -> bool:
        inividual_existence = list(map(lambda x: x.exists(), self))
        collective_existence = reduce((lambda x, y: x and y), inividual_existence)
        return collective_existence

    ### LIST handling
    def append(self, an_object: TFile):
        self._check(an_object)
        super(TFileList, self).append(an_object)

    def __setitem__(self, i, an_object):
        self._check(an_object)
        super(TFileList, self).__setitem__(i, an_object)

    def insert(self, i, an_object):
        self._check(an_object)
        super(TFileList, self).insert(i, an_object)

    def _check(self, item: TFile):
        if not isinstance(item, TFile):
            raise ValueError('Wrong input type, here should be only TFile instances')


class TFolderList(list):

    def sort_human_way(self):
        ai_fn.sort_human_way(self)

    def __init__(self, folders: list):
        as_TFolder = [TFolder(item) for item in folders]
        super(TFolderList, self).__init__(as_TFolder)
        # can be aceesed as a list, through inheritance

        # verify that all of elements are TFolder type!!!
        for item in self:
            self._check(item)

    def exists(self) -> bool:
        inividual_existence = list(map(lambda x: x.exists(), self))
        collective_existence = reduce((lambda x, y: x and y), inividual_existence)
        return collective_existence

    ### LIST handling
    def append(self, an_object: TFolder):
        self._check(an_object)
        super(TFolderList, self).append(an_object)

    def __setitem__(self, i, an_object):
        self._check(an_object)
        super(TFolderList, self).__setitem__(i, an_object)

    def insert(self, i, an_object):
        self._check(an_object)
        super(TFolderList, self).insert(i, an_object)

    def _check(self, item: TFolder):
        if not isinstance(item, TFolder):
            raise ValueError('Wrong input type, here should be only TFolder instances')


# low level config only for utility_types, as we cannot use config of a higher level
class LowLevelConfig(object):
    def __init__(self, config_filename: str='config_utils.json'):
        """
        Most low level config
        :param config_filename:
        """
        # reads configuration json
        install_directory = os.path.dirname(os.path.abspath(__file__))
        self._source_code_directory = install_directory
        # reads configuration json
        self._key_val_map = {}
        self._config_filename = os.path.join(install_directory, config_filename)
        with open(self._config_filename) as f:
            self._key_val_map = json.load(f)

        self._module_tests_data = self._key_val_map['module_tests_data']

    @property
    def utility_types_tests_data(self) -> dict:
        return self._module_tests_data

if __name__ == '__main__':

    print('Simple checks')
    # base_config = LowLevelConfig()
    # root_folder = base_config.utility_types_tests_data['root_path']
    # output_folder = os.path.join(root_folder, base_config.utility_types_tests_data['utility_types']['output'])
    #
    # output_folder_t = TFolder(output_folder)
    # print(output_folder_t)
    #
    # next_folder = output_folder_t.add_sub_path('test_path')
    # next_folder.make()

    vector = TVector([5, 6, 7])
    size = TSize([2, 7, 10])
    summ = vector + size  # + size
    print(summ)
    print(isinstance(summ, TVector), isinstance(summ, TSize), isinstance(summ, np.ndarray)) # ???

