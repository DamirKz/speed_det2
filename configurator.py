# Principles:
# Test driven development
# Minimal complexity
# Write for maintenace
# Loose coupling
# information hiding
# if comments are a problem I'll use a script to clean them, just need to establish written coventions

import json
import os
from utils import utility_types
import random
import numpy as np

class Configurator:  # all that is necessary for usage is in the code
    def __init__(self, config_filename: str = 'config.json'):
        # reads configuration json
        install_directory = utility_types.TFolder(os.path.dirname(os.path.abspath(__file__)))
        self._source_code_directory = install_directory
        # reads configuration json
        self._key_val_map = {}
        self._config_filename = os.path.join(install_directory, config_filename)
        with open(self._config_filename) as f:
            self._key_val_map = json.load(f)

        # meaningful fields
        self.__data = self._key_val_map['input_data']

        # set test data path
        self._module_tests_data = self._key_val_map['module_tests_data']

        # init random for debug
        if 'random_seed' in self._key_val_map.keys():
            random_seed = self._key_val_map['random_seed']
            random_seed = np.random.randint(10000)
            print('DEBUG random init: ', True)
            random.seed(random_seed)
            np.random.seed(random_seed)
            print('RANDOM INIT, value: ', random_seed)

    @property
    def input_root(self) -> utility_types.TFolder:
        return utility_types.TFolder(self.__data)

    @property
    def module_tests_root(self) -> utility_types.TFolder:
        return utility_types.TFolder(self._module_tests_data['root_path'])

    @property
    def module_tests_data(self) -> dict:
        return self._module_tests_data

    def get_all_module_tests_dirs(self, config_field: str)  -> (utility_types.TFolder,
                                                           utility_types.TFolder,
                                                           utility_types.TFolder):
        """
        Usage:
          input_folder, output_folder, debug_folder = Config.get_all_module_tests_dirs('image_types')
        :param config_field:
        :return: input, output, DEBUG for a specific input data
        """
        root_input = self.module_tests_root
        input_folder = root_input.add_sub_path(self.module_tests_data[config_field]['input'])
        output_folder = root_input.add_sub_path(self.module_tests_data[config_field]['output'])
        debug_folder = root_input.add_sub_path(self.module_tests_data[config_field]['DEBUG'])

        return input_folder, output_folder, debug_folder


# it will automatically Singletone, as modules are imported only once
Config = Configurator()

if __name__ == '__main__':

    input_data_root = Config.input_root
    print('input data root: ', input_data_root)


    assert input_data_root.exists(), 'Input directory should exist'


    # do_some_stuff.py - ----------------------------------
    # from configurator import Config

    # Powershell line count:
    # dir -Include *.py -Recurse |
    #    % { $_ | select name, @{n="lines";e={
    #        get-content $_ |
    #          measure-object -line |
    #              select -expa lines }
    #                                        }
    #      } | ft -AutoSize

