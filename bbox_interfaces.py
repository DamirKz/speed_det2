import numpy as np


class TBbox(list):
    """
    Single bounding box
            Axis directions
                                       y
                 ----------------------
                 |
                 |
                 |    Image 0
               x |
                 |
    x1y1_x2y2_coordiantes: should be in the form [ [x1, y1], [x2, y2]]
    """
    @classmethod
    def zero(cls):
        return [[], []]

    @property
    def upper_left(self):
        return np.array(self[0])

    @property
    def bottom_right(self):
        return np.array(self[1])


class TSymbolBbox(TBbox):
    """
    Bbox for symbols, contains bbox and value, that can contain several symbols.
    """
    """
    If symbol is unknown it is represented as *.
    """
    @classmethod
    def zero(cls):
        return cls([[], []], '')

    def __init__(self, bbox_x1y1_x2y2: list,
                       value: str):
        super(TSymbolBbox, self).__init__(bbox_x1y1_x2y2)
        assert len(self) == 2, 'There should be 2 sublists with x,y coordinates'
        self.value = value


class TSymbolBboxList(list):

    @classmethod
    def zero(cls):
        return cls([])

    def __init__(self, symbol_bboxes: list):
        super(TSymbolBboxList, self).__init__(symbol_bboxes)
        # can be accesed as a list, through inheritance

        # verify that all of elements are TSymbolBbox type!!!
        for item in self:
            self._check(item)

    ### LIST handling
    def append(self, an_object: TSymbolBbox):
        self._check(an_object)
        super(TSymbolBboxList, self).append(an_object)

    def __setitem__(self, i, an_object):
        self._check(an_object)
        super(TSymbolBboxList, self).__setitem__(i, an_object)

    def insert(self, i, an_object):
        self._check(an_object)
        super(TSymbolBboxList, self).insert(i, an_object)

    def _check(self, item: TSymbolBbox):
        if not isinstance(item, TSymbolBbox):
            raise ValueError('Wrong input type, here should be only TSymbolBbox instances')


class TCompositeSymbolsBbox(TSymbolBbox):

    @classmethod
    def zero(cls):
        return cls(TSymbolBbox.zero(), TSymbolBboxList.zero())

    def __init__(self, symbol_bbox: TSymbolBbox,
                       content: TSymbolBboxList):
        super(TCompositeSymbolsBbox, self).__init__(symbol_bbox, symbol_bbox.value)
        self._individual_bboxes = content

    def individual_bboxes(self) -> TSymbolBboxList:
        return self._individual_bboxes

    def __str__(self):
        start = '\n-------------------\n'
        overall_bbox = 'Overall_bbox: ' + super.__str__(self) + ' Value: ' + self.value + '\n'
        inside_bbox = 'symbol_bboxes_inside: ' + str(self._individual_bboxes)
        symbols_inside = []
        for s_bbox in self._individual_bboxes:
            current_value = s_bbox.value
            symbols_inside.append(current_value)
        end = '\n-------------------\n'
        return start + overall_bbox + inside_bbox + '\n' + 'Values: ' + str(symbols_inside) + end

if __name__ == '__main__':

    print('TSymbol bbox')
    symbol_bbox = TSymbolBbox([[10, 10], [20, 20]], '1')
    print(symbol_bbox)
    print(symbol_bbox.value)
    symbol_bbox2 = TSymbolBbox([[10, 10], [20, 20]], '2')
    symbol_bbox3 = TSymbolBbox([[20, 20], [20, 20]], '')
    print(symbol_bbox2)
    bbox3 = TBbox([[10, 10], [20, 20]])

    bboxes = [symbol_bbox, symbol_bbox2, symbol_bbox3]
    print(bboxes)

    type_bboxes = TSymbolBboxList(bboxes)
    print(type_bboxes)
    for a_bbox in type_bboxes:
        print(a_bbox.value)



    overall_bbox = TSymbolBbox([[10, 10], [50, 50]], '12')
    print(overall_bbox, overall_bbox.value)

    composite_bbox = TCompositeSymbolsBbox( overall_bbox,
                                            type_bboxes)

    print('Composite: ', composite_bbox)

    print('Separate access: ')
    print('Oberall_bbox: ', composite_bbox._individual_bboxes)
    print('Value', composite_bbox.value)
    print('inside', composite_bbox._individual_bboxes)