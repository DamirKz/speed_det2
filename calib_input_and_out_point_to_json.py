# -*- coding: utf-8 -*-
from utils.utility_types import TVector, TImagePoint, TFolder, TFile
from configurator import Config
import numpy as np
import cv2
import os
import time
import _pickle as cPickle
#import pickle
import sys
import matplotlib.pyplot as plt
import json

# ------------- fun
def loadCache(cacheFile):
    with open(cacheFile, 'rb') as fid:
        if sys.version_info.major > 2:
            return cPickle.load(fid, encoding = 'latin1')
        else:
            return cPickle.load(fid)

def show_lines_points_distances(image_mat: np.ndarray, distances, input_points1, input_points2, size=15, color = (255,255,0)):

    input_points1 = np.float32(input_points1)
    input_points2 = np.float32(input_points2)
    num, dim = input_points1.shape
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontColor = (255, 0, 255)
    lineType = 2

    list_points = []
    for i in range(num):
            cv2.circle(image_mat, (input_points1[i][0], input_points1[i][1]), 5, (255,255,0), -1)
            #print(i, (input_points1[i][0], input_points1[i][1]) )
            cv2.circle(image_mat, (input_points2[i][0], input_points2[i][1]), size, (0,255,255), -1)
            #print(i, (input_points2[i][0], input_points2[i][1]))
            cv2.line(image_mat, (input_points1[i][0], input_points1[i][1]), (input_points2[i][0], input_points2[i][1]), fontColor, 2)
            #print(distances[i])
            bottomLeftCornerOfText = (int((input_points1[i][0]+input_points2[i][0])/2), int((input_points2[i][1]+input_points2[i][1])/2))
            cv2.putText(image_mat, str(distances[i]), bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
            list_points.append(input_points1[i][0])
            list_points.append(input_points1[i][1])
            list_points.append(input_points2[i][0])
            list_points.append(input_points2[i][1])

    return list_points

def show_image(image_mat:np.ndarray):
    while (1):
        cv2.imshow('image', image_mat)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()

class CoordinateStore:
    def __init__(self, img):
        self.points = []
        self.img = img

    def select_point(self,event,x,y,flags,param):
            if event == cv2.EVENT_LBUTTONDBLCLK:
                cv2.circle(self.img,(x,y),3,(255,0,0),-1)
                self.points.append((x,y))

def show_points(image_mat: np.ndarray, input_points, size=15, color = (255,255,0)):

    if len(input_points) > 0:
        input_points = np.float32(input_points)
        num, dim = input_points.shape
        if num > 0:
            for i in range(num):
                # print('Show!')
                cv2.circle(image_mat, (input_points[i][0], input_points[i][1]), size, color, -1)

def takeClosest(num,collection):
   return min(collection,key=lambda x:abs(x-num))


if __name__ == '__main__':

    look = ['_center', '_left', '_right']
    list_dir = []
    for i in range(7):
        for j in range(3):
            list_dir.append('session'+ str(i)+look[j])
    print(list_dir)

    name_session = list_dir[7]
    print(name_session)

    name_session = 'session5_center'

    path = 'D:/VideosetsBRNO/BrnoCompSpeed-full/BrnoCompSpeed/dataset/'
    png_filename = os.path.join(path, name_session,'screen.png')
    dict_filename = os.path.join(path, name_session, 'gt_data.pkl')
    #json_filename = os.path.join(path, name_session, 'gt_data.json')
    calib_points_json = os.path.join(path, name_session, 'calib_points.json')

    print(png_filename)

    image_mat = cv2.imread(png_filename)
    cv2.namedWindow('image')
    coordinateStore1 = CoordinateStore(image_mat)
    gtData = loadCache(dict_filename)

    points_p2, points_p1, distances = [], [], []
    for keys in gtData['distanceMeasurement']:
        points_p2.append(keys['p2'])
        points_p1.append(keys['p1'])
        distances.append(keys['distance'])

    list_points = show_lines_points_distances(image_mat, distances, points_p1, points_p2)

    cv2.setMouseCallback('image',coordinateStore1.select_point)
    show_image(image_mat)

    cv2.imwrite(os.path.join(path,name_session,'screen_marked.png'), image_mat)

    new_coor = []
    for coor in coordinateStore1.points:
        new_coor.append((int(takeClosest(coor[1], list_points)), int(takeClosest(coor[0], list_points))))
    print('corrected_coor:', new_coor)

    calib_points = {}
    rect_x = 300
    rect_y = 300

    # return standart points (numpy)

    keys = ['input_points', 'output_points', 'rect', 'xy_m', 'fps']
    calib_points[keys[0]] = new_coor
    calib_points[keys[1]] = [[rect_x, 0], [0, 0], [0, rect_y], [rect_x, rect_y]]
    calib_points[keys[2]] = [rect_x, rect_y]
    calib_points[keys[3]] = [28/rect_x, 7.97/rect_y]
    calib_points[keys[4]] = 50

    print(calib_points)

    with open(calib_points_json, "w") as data_file:
        json.dump(calib_points, data_file, indent=4)

