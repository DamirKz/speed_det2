from configurator import Config
import os
import json
import cv2
import numpy as np


if __name__ == '__main__':

    print('Cars tracking')

    root_input = Config.module_tests_root
    input_dir, output_dir, debug_dir = Config.get_all_module_tests_dirs('cars_tracking')

    name_sample_dir = 'session3_left'
    sample_json = 'video_6m43s_6m56s.avi.lprlog.json'
    sample_avi = 'video_6m43s_6m56s.avi'
    # ---------- !
    time_of_cut = 6*60 + 43
    fps = 50
    # ----------
    sample_output_dir = output_dir.add_sub_path(name_sample_dir).make()
    sample_debug_folder = debug_dir.add_sub_path(name_sample_dir).make()
    sample_json_path = os.path.join(input_dir, name_sample_dir, sample_json)
    sample_avi_path = os.path.join(input_dir, name_sample_dir, sample_avi)

    print(input_dir)
    print(output_dir)
    print(debug_dir)
    print(sample_output_dir)
    print(sample_debug_folder)
    print(sample_json_path)
    print(sample_avi_path)

    # count number of frames
    cap = cv2.VideoCapture(sample_avi_path)
    k = 0
    while cap.isOpened():
        _, image_mat = cap.read()
        if _:
            k = k + 1
        else:
            break
    cap.release()
    print(k)

    # count number of IDs
    all_info = {}
    MAX = -1
    with open(sample_json_path) as f:
        lines = f.readlines()
        for ib in range(1, k+1):
            key = json.loads(lines[ib])
            for jb in range(len(key['Value'])):
                ID_Bbox = key['Value'][jb]['Bbox']
                if int(ID_Bbox['ID']) > MAX:
                    MAX = int(ID_Bbox['ID'])   # num ID

    print('MAX = ', MAX)
    all_ids = []
    for i in range(MAX+1):
        all_ids.append([])

    shift_frame = time_of_cut*fps
    # sort input json by ID
    with open(sample_json_path) as f:
        lines = f.readlines()
        for ib in range(1, k + 1):
            key = json.loads(lines[ib])
            for jb in range(len(key['Value'])):
                ID_Bbox = key['Value'][jb]['Bbox']
                for i in range(MAX+1):
                    if int(ID_Bbox['ID']) == i:
                        ID_Bbox['origin_frame'] = key['Key']
                        ID_Bbox['shifted_frame'] = key['Key'] + shift_frame
                        ID_Bbox['time'] = (key['Key'] + shift_frame)/fps
                        all_ids[i].append(ID_Bbox)

    for i in range(len(all_ids)):

        name = 'ID' + str(i) + '.json'
        out_json = os.path.join(sample_output_dir, name)
        with open(out_json, "w") as data_file:
            json.dump(all_ids[i], data_file, indent=4)

    # -------------------------------------------------------------------------

    #print(key)
    #print(key[''])
    # all_ids = []
    # for jb in range(len(key['Value'])):
    #     ID_Bbox = key['Value'][jb]['Bbox']
    #     all_ids.append(ID_Bbox)
    # all_info[key['Key']] = all_ids

    # with open(output_json, "w") as data_file:
    #     json.dump(all_info, data_file, indent=4)




    #
    # all_files = os.listdir(input_folder)
    # print(all_files)

    # ID = 4
    #
    #
    # name_session = 'session2_left/'
    # path = 'D:/VideosetsBRNO/BrnoCompSpeed-full/BrnoCompSpeed/dataset/'
    # filename = path + name_session + 'Part1_364.avi.lprlog.json'
    # filename_video = path + name_session + 'Part1_364.avi'
    # sample_name = path + name_session + 'Part1_364.json'
    # ID_name_json = path + name_session + 'Part1_364_ID4.json'
    #
    # color = (255, 255, 0)
    # thickness = 2
    #
    # all_info = {}
    # with open(filename) as f:
    #     lines = f.readlines()
    #     for ib in range(1, len(lines)):
    #
    #         key = json.loads(lines[ib])
    #         all_ids = []
    #         for jb in range(len(key['Value'])):
    #             ID_Bbox = key['Value'][jb]['Bbox']
    #             all_ids.append(ID_Bbox)
    #         all_info[key['Key']] = all_ids
    #
    # with open(sample_name, "w") as data_file:
    #     json.dump(all_info, data_file, indent=4)
    #
    #
    # cap = cv2.VideoCapture(filename_video)
    # k = 0
    # info_4ID = {}
    # track_list = []
    # while cap.isOpened():
    #     _, image_mat = cap.read()
    #     k = k + 1
    #     print(k)
    #     if k in all_info:
    #         for el in all_info[k]:
    #             if int(el['ID']) == 4:
    #                 info_4ID[k] = el
    #                 cv2.rectangle(image_mat, (int(el['X']), int(el['Y'])),
    #                               (int(el['X'] + el['Width']), int(el['Y']+el['Height'])),
    #                               color, thickness)
    #                 track_list.append( [int(el['X']+el['Width']/2), int(el['Y']+el['Height']/2)] )
    #                 for indx in track_list:
    #                     cv2.circle(image_mat,(indx[0], indx[1]), 2, (255,0,255), -1)
    #                     #cv2.circle(image_mat, (int(el['X']+el['Width']/2), int(el['Y']+el['Height']/2)), 5, (255,0,255), -1)
    #     if _:
    #         cv2.waitKey(100)
    #         cv2.imshow("Frame", image_mat)
    #     else:
    #         break
    #
    # cap.release()
    # cv2.destroyAllWindows()
    #
    # with open(ID_name_json, "w") as data_file:
    #     json.dump(info_4ID, data_file, indent=4)
    #
    #
