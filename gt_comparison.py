from configurator import Config
import os
import json
import cv2
import numpy as np

if __name__ == '__main__':

    print('GT Comparison')

    root_input = Config.module_tests_root
    input_dir, output_dir, debug_dir = Config.get_all_module_tests_dirs('gt_comparison')

    name_sample_dir = 'session3_left'
    input_ID_json  = 'ID2_speed.json' # input json with tracking ID car
    gt_input = 'gt_speed_data.json'
    output_ID_json = os.path.splitext(input_ID_json)
    output_ID_json = output_ID_json[0]+'_gt' + output_ID_json[1]
    show_id = True
    fps = 50

    sample_output_folder = output_dir.add_sub_path(name_sample_dir).make()
    sample_debug_folder = debug_dir.add_sub_path(name_sample_dir).make()
    sample_input_folder = os.path.join(input_dir, name_sample_dir)

    input_ID_path = os.path.join(sample_input_folder, input_ID_json)
    output_ID_path = os.path.join(sample_output_folder, output_ID_json)
    gt_input_path = os.path.join(sample_input_folder, gt_input)

    print(' --------  Base Dir -------------')
    print(input_dir)
    print(output_dir)
    print(debug_dir)
    print(' --------  Input, Output, Debag Dirs -------------')
    print(sample_input_folder)
    print(sample_output_folder)
    print(sample_debug_folder)
    print(' --------  Input, Output, Debag Files -------------')
    print(input_ID_path)
    print(output_ID_path)
    print(gt_input_path)

    # load json with calibration data
    with open(input_ID_path) as f:
        info_speed = json.load(f)

    print(info_speed[0]['frame']/fps)

    with open(gt_input_path) as f:
        info_gt = json.load(f)

    #print(info_gt)