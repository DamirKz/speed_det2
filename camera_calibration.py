# -*- coding: utf-8 -*-
from utils.utility_types import TVector, TImagePoint, TFolder, TFile
from configurator import Config
import numpy as np
import cv2
import os
import time
import _pickle as cPickle
#import pickle
import sys
import matplotlib.pyplot as plt
import json

class ICameraCalibration(object):
    """
    Interface for camera calibration

    Usage:
    custom_calib = CustomCameraClaibration()
    custom_calib.calibrate_by_image_and_4points(image_mat: np.ndarray, pts1: list, pts2: list, rect: list)
    transformed_point = custom_calib.transform_to_real(input_point)

    """
    def calibrate_by_image_and_4points(self, image_mat: np.ndarray, pts1: list, pts2: list, rect: list):
        raise NotImplementedError

    def transform_to_real(self, image_point: TImagePoint) -> TVector:
        raise NotImplementedError

    def get_HomographyMatrix(self, pts1: list, pts2: list, rect: list) -> np.ndarray:
        raise NotImplementedError

class Simple2DCalibration(ICameraCalibration):

    def load_from_file(self, path_to_calibration_json: TFile):
        raise NotImplementedError

    def load_from_homography(self, path_to_homography_np: TFile):
        raise NotImplementedError

    def __init__(self, is_debug: bool = False, debug_folder: TFolder = TFolder.zero()):

        self._is_debug = is_debug
        self._debug_folder = debug_folder
        self._points = []
        self._image_mat = np.ndarray([])  #
        self._image_new = np.ndarray([])
        self._type_trans = 'standart'
        self.M = np.ndarray([])
        self.dst = np.ndarray([])
        self.M_i = np.ndarray([])

    # 1
    def calibrate_by_image_and_4points(self, image_mat: np.ndarray, pts1: list, pts2: list, rect: list):

        self._image_mat = image_mat
        self.get_HomographyMatrix(pts1, pts2, rect)
        self.rows, self.cols, self.ch = self._image_mat.shape
        self._get_size_transformed_matrix()

    # 2
    def get_HomographyMatrix(self, pts1: list, pts2: list, rect: list):

        pts1 = np.float32(CoordinateTransform.f_standart_to_cv2(pts1))
        pts2 = np.float32(CoordinateTransform.f_standart_to_cv2(pts2))
        self.M = cv2.getPerspectiveTransform(pts1, pts2)
        # self.dst = cv2.warpPerspective(self._image_mat, self.M, tuple(rect))
        # self.M_i = np.linalg.inv(self.M)

    # 3
    def _get_size_transformed_matrix(self):

        c_x, c_y = [], []
        for ox in range(self.rows):  # 352
            for oy in range(self.cols):  # 500
                px, py = self._transform_point_to_point(ox, oy)
                c_x.append(px)
                c_y.append(py)

        self._minx = int(abs(min(c_x)))
        self._miny = int(abs(min(c_y)))
        self._maxx = int(max(c_x))
        self._maxy = int(max(c_y))      # print(min(c_x), min(c_y), maxx, maxy)


    # 4
    def transform_img(self):

        self._im_new = np.zeros([self._minx + self._maxx + 1, self._miny + self._maxy + 1, self.ch], dtype=np.uint8)
        for ox in range(self.rows):
            for oy in range(self.cols):
                px, py = self._transform_point_to_point(ox, oy)
                x1 = px + self._minx
                y1 = py + self._miny
                self._im_new[x1, y1] = self._image_mat[ox, oy]

        if self._is_debug:
            self._debug_output(self._im_new)         #return self._im_new

    # 5
    def _transform_point_to_point(self, ox, oy): # transform point in standart coordinate and return standart coordinate

        if self._type_trans == 'standart':
            ar = np.float32([oy, ox, 1])
            p = np.matmul(self.M, ar)
            px, py = int(np.around(p[0] / p[2])), int(np.around(p[1] / p[2]))
            return py, px
        elif self._type_trans == 'cv2':
            ar = np.float32([ox, oy, 1])
            p = np.matmul(self.M, ar)
            px, py = int(np.around(p[0] / p[2])), int(np.around(p[1] / p[2]))
            return px, py

    # 6
    def transform_xy_to_real(self, image_point: TImagePoint) -> TVector:

        sd = np.float32(image_point)
        ox, oy = sd[0], sd[1]
        px, py = self._transform_point_to_point(ox, oy)
        output = [px + self._minx, py + self._miny, 1]
        return TVector(output)

    # auxiliary function

    def get_shift(self):
        return [self._minx, self._miny]

    def _debug_output(self, image_mat: np.ndarray):
        print('DEBUG: Saving debug calibrated image')
        debug_output_filename = 'debug.png'
        debug_output_filename = self._debug_folder.add_file_name(debug_output_filename)
        cv2.imwrite(debug_output_filename, image_mat)

class CoordinateTransform(object):

    def __init__(self):
        self.standart_points = []
        self.cv2_points = []
        self._img = np.ndarray([])

    def select_point(self,event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDBLCLK:
            cv2.circle(self._img,(x,y),3,(255,0,0),-1)
            self.cv2_points.append(TImagePoint([x, y]))
            self.standart_points.append(TImagePoint([y, x]))

    def set_tops_rectangle(self, img: np.ndarray) -> list:  # list of TImagePoint

        self._img = img
        cv2.namedWindow('image')
        self.cv2_points = []
        cv2.setMouseCallback('image', self.select_point)
        while (1):
            cv2.imshow('image', self._img)
            k = cv2.waitKey(20) & 0xFF
            if k == 27:
                break
        cv2.destroyAllWindows()
        return self.cv2_points


    @staticmethod
    def f_standart_to_cv2(input_points: list) -> list:

        cv2_points = []
        for ind in input_points:
            x, y = ind[0], ind[1]
            cv2_point = TImagePoint([y, x])
            cv2_points.append(cv2_point)

        return cv2_points

    @staticmethod
    def f_cv2_to_standart(input_points: list) -> list:

        standart_points = []
        for ind in input_points:
            x, y = ind[0], ind[1]
            standard_point = TImagePoint([y, x])
            standart_points.append(standard_point)

        return standart_points

    @staticmethod
    def f_to_TImagePoint(input_points: list) -> list:

        standart_points = []
        for ind in input_points:
            x, y = ind[0], ind[1]
            standard_point = TImagePoint([x, y])
            standart_points.append(standard_point)

        return standart_points

# ------------   auxiliary function

def show_image(image_mat:np.ndarray):
    while (1):
        cv2.imshow('image', image_mat)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()


def show_points(image_mat: np.ndarray, input_points, size=15, color = (255,255,0)): # input_points - standart

    if len(input_points) > 0:
        input_points = np.float32(input_points)
        num, dim = input_points.shape
        if num > 0:
            for i in range(num):
                cv2.circle(image_mat, (input_points[i][1], input_points[i][0]), size, color, -1)


def show_lines_points_distances(image_mat: np.ndarray, distances, input_points1, input_points2, size=15, color = (255,255,0)):

    input_points1 = np.float32(input_points1)
    input_points2 = np.float32(input_points2)
    num, dim = input_points1.shape
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontColor = (255, 0, 255)
    lineType = 2

    for i in range(num):
        cv2.circle(image_mat, (input_points1[i][0], input_points1[i][1]), size, color, -1)
        cv2.circle(image_mat, (input_points2[i][0], input_points2[i][1]), size, color, -1)
        cv2.line(image_mat, (input_points1[i][0], input_points1[i][1]), (input_points2[i][0], input_points2[i][1]), fontColor, 2)
        bottomLeftCornerOfText = (int((input_points1[i][0]+input_points2[i][0])/2), int((input_points2[i][1]+input_points2[i][1])/2))
        cv2.putText(image_mat, str(distances[i]), bottomLeftCornerOfText, font, fontScale, fontColor, lineType)


if __name__ == '__main__':


    name_sample = 'session3_left'
    info_ = {}
    calibrated_input_points = []
    show_im = True

    input_dir, output_dir, debug_dir = Config.get_all_module_tests_dirs('camera_calibration')
    sample_output_dir = output_dir.add_sub_path(name_sample).make()
    sample_debug_folder = debug_dir.add_sub_path(name_sample).make()
    png_filename = os.path.join(input_dir, name_sample, 'screen.png')
    calibration_json_filename = os.path.join(sample_output_dir, 'Info_Calibration.json')
    calib_points_json = os.path.join(input_dir, name_sample, 'calib_points.json')

    print(input_dir, output_dir, debug_dir)
    print(sample_output_dir)
    print(sample_debug_folder)
    print(png_filename)
    print(calibration_json_filename)
    print(calib_points_json)



    with open(calib_points_json) as f:
        calib_points = json.load(f)

    standart_input_points = CoordinateTransform.f_to_TImagePoint(calib_points['input_points'])
    standart_output_points = CoordinateTransform.f_to_TImagePoint(calib_points['output_points'])
    image_mat = cv2.imread(png_filename)

    if show_im == True:
        show_points(image_mat, standart_input_points, size = 15, color = (255, 255, 0))
        show_image(image_mat)

    # print(standart_input_points, standart_output_points, rect)

    first_calibrator = Simple2DCalibration(True, sample_debug_folder)
    first_calibrator.calibrate_by_image_and_4points(image_mat,
                                                    standart_input_points,
                                                    standart_output_points,
                                                    calib_points['rect'])

    for Points in standart_input_points:
        calibrated_input_points.append(first_calibrator.transform_xy_to_real(Points).as_list)

    info_['HomographyMatrix'] = first_calibrator.M.tolist()
    info_['Shift'] = first_calibrator.get_shift()
    info_['Transformed_Input_Points'] = calibrated_input_points
    info_['Input_Points'] = calib_points['input_points']
    info_['Output_Points'] = calib_points['output_points']
    info_['Rect'] = calib_points['rect']
    info_['XY_m'] = calib_points['xy_m']
    info_['fps'] = calib_points['fps']

    with open(calibration_json_filename, "w") as data_file:
        json.dump(info_, data_file, indent=4)

    #first_calibrator.transform_img()



